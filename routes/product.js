//Dependencies and Modules
const express = require('express');
const productController = require('../controllers/product');
const auth = require('../auth');

const { verify, verifyAdmin } = auth;

//Routing Component
const router = express.Router();


// Route for creating a product. (Admin Only)
router.post("/addProduct", verify, verifyAdmin, productController.addProduct);


// Route for retrieving all products. (Admin Only)
router.get("/all", productController.getAllProduct);


// Route for retrieving all the ACTIVE products.
router.get("/allActive", productController.getAllActive);


// Route to search for products by product name
router.post('/search', productController.searchProductsByName);


// Search Products By Price Range
router.post('/searchByPrice', productController.searchProductsByPriceRange);


// Route for retrieving single product.
router.get("/:productId", productController.getProduct);


// Route for updating a product. (Admin Only)
router.put("/:productId/update", verify, verifyAdmin, productController.updateProduct);


// Route to archiving a product (Admin Only)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);


// Route to activating a product (Admin Only)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);


module.exports = router;