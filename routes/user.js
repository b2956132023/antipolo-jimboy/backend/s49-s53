const express = require('express');
const userController = require('../controllers/user');
const auth = require('../auth');

const { verify, verifyAdmin } = auth;

const router = express.Router();

// Route for checking if user email already exist.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", userController.registerUser);


// Route for user login
router.post("/login", userController.loginUser);


// Route to checkout a product by a Non-Admin User.
router.post("/checkout", verify, userController.checkout);


// Router to retrieve the user details
router.get("/details", verify, userController.getProfile);


// Router to set user as admin. (Admin Only);
router.put("/verify", verify, verifyAdmin, userController.verifyAsAdmin);


// Router to retrieve the order details
router.get("/getOrder", verify, userController.retrieveOrder);


// Route for resetting the password
router.put('/reset-password', verify, userController.resetPassword);


// Update user profile route
router.put('/profile', verify, userController.updateProfile);


// Router to retrieve all orders. (Admin Only)
router.get("/getAllOrder", verify, verifyAdmin, userController.retrieveAllOrder);


// Router to add product to cart.
// router.post("/addToCart", verify, userController.addToCart);



module.exports = router;