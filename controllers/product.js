const Product = require("../models/Product");
const User = require("../models/User");


// Saves the created product to database
module.exports.addProduct = (req, res) => {

	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});

	return newProduct.save().then((product, error) => {

		if(error) {
			return res.send(false);

		} else {
			return res.send({message: "Product Registered!"});
		}
	})
	.catch(err => res.send(err))
};


// Retrieve all products
module.exports.getAllProduct = (req, res) => {
	return Product.find({}).then(result => {

		return res.send(result);
	})
	.catch(err => res.send(err))
};


// Retrieve All ACTIVE Products
module.exports.getAllActive = (req, res) => {
	return Product.find({ isActive: true }).then(result => {

		return res.send(result)
	})
	.catch(err => res.send(err))
};


// Controller action to search for products by product name
module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const products = await Product.find({
      name: { $regex: productName, $options: 'i' }
    });

    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


// Controller action to search for products by price range

module.exports.searchProductsByPriceRange = async (req, res) => {
     try {
       const { minPrice, maxPrice } = req.body;
  
          // Find products within the price range
       const products = await Product.find({
        price: { $gte: minPrice, $lte: maxPrice }
      });
      
      res.json(products);
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while searching for products' });
    }
   };


// Retrieve single Product
module.exports.getProduct = (req, res) => {
	return Product.findById(req.params.productId).then(result => {
		console.log(result)
		return res.send(result)
	})
	.catch(err => res.send(err))
};


// Update Product
module.exports.updateProduct = (req, res) => {

	let updatedProduct = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {

		if(error) {
			return res.send(false);

		} else {
			return res.send({message: "Product Successfully Updated!"});
		}
	})
	.catch(err => res.send(err))
};


// Archive a product
module.exports.archiveProduct = (req, res) => {

	let updateActiveField = {
		isActive: false
	}

	return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
	.then((product, error) => {

		if(error){
			return res.send(false)

		} else {
			return res.send({message: "Product Successfully Archived!"})
		}
	})
	.catch(err => res.send(err))

};


// Activate a product
module.exports.activateProduct = (req, res) => {

	let updateActiveField = {
		isActive: true
	}

	return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
	.then((product, error) => {

		if(error){
			return res.send(false)

		} else {
			return res.send({message: "Product Successfully Activated!"})
		}
	})
	.catch(err => res.send(err))

};