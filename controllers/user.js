const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		// The "find" metho
		// returns a record if a match is found.
		if(result.length > 0) {
			return true; //"Duplicate email found"
		} else {
			return false;
		}
	})
};


// [SECTION] User Registration
module.exports.registerUser = (req, res) => {	

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return res.send(false);

		} else {
			return res.send(true);
		}
	})
	.catch(err => res.send(err));
};


// [SECTION] User Login
module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => {

		if(result == null) {
			return false
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			if(isPasswordCorrect) {
				return res.send({access: auth.createAccessToken(result)})
			} else {
				return res.send(false);
			}
		}
	})
	.catch(err => res.send(err));
};


// [SECTION] Checkout a product by a Non-Admin User.
module.exports.checkout = async (req, res) => {
  if (req.user.isAdmin) {
    return res.send({ message: "Admin can not use this action." });
  }

  try {
    let user = await User.findById(req.user.id);

    let product = await Product.findById(req.body.productId);

    let newCheckout = {
    	products:[{
    		productId: req.body.productId,
    		name: req.body.name,
    		quantity: req.body.quantity
    	}],      
      totalAmount: req.body.quantity * product.price
    };

    user.orderedProduct.push(newCheckout);
    await user.save();

    let notAdmin = {
      userId: req.user.id,
    };

    product.userOrders.push(notAdmin);
    await product.save();

    return res.send(true);
  } catch (error) {
    return res.send({ message: error.message });
  }

};


// [SECTION] Retrieve the user details
module.exports.getProfile = (req, res) => {

	return User.findById(req.user.id).then(result => {

		result.password = "";

		return res.send(result);
	})
	.catch(err => res.send(err))
};


// [SECTION] Set user as admin
module.exports.verifyAsAdmin = (req, res) => {

  let updateAsAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(req.body.userId, updateAsAdmin)
	.then((product, error) => {

		if(error){
			return res.send(false)

		} else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
};

// Function to reset the password
module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json(true);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};



module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, email } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, email },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
};


// [SECTION] Retrieve the order details
module.exports.retrieveOrder = (req, res) => {

	return User.findById(req.user.id).then(result => {

		return res.send(result.orderedProduct);
	})
	.catch(err => res.send(err))
};


// [SECTION] Retrieve all orders
module.exports.retrieveAllOrder = (req, res) => {
    
    const allOrderedProducts = [];

    User.find({}).then(results => {

      results.forEach(user => {

        allOrderedProducts.push(user.orderedProduct);
      });
      return res.send(allOrderedProducts);
  })
  .catch(err => res.send(err));
};

// [SECTION] Add a product to cart.
// module.exports.addToCart = async (req, res) => {
//   if (req.user.isAdmin) {
//     return res.send({ message: "Admin can not use this action." });
//   }

//   try {
//     let user = await User.findById(req.user.id);

//     let product = await Product.findById(req.body.productId);

//     let subTotalArray = [];

//     user.orderedProduct.find({}).then(results => {

//       results.forEach(user => {

//        subTotalArray.push(user.orderedProduct[1]);
//       });
//   	})  

//     const sumSubtotal = subTotalArray.reduce((acc, curr) => acc + curr, 0);

//     let newAddToCart = {
//       product:[{
//     		productId: req.body.productId,
//     		productName: req.body.productName,
//     		quantity: req.body.quantity,
//     		subTotal: req.body.quantity * product.price
//     	}],      
//       totalAmount: sumSubtotal
//     };

//     user.cart.push(newAddToCart);
//     await user.save();

//     let notAdmin = {
//       userId: req.user.id,
//     };

//     product.userCart.push(notAdmin);
//     await product.save();

//     return res.send(true);
//   } catch (error) {
//     return res.send({ message: error.message });
//   }
// };
