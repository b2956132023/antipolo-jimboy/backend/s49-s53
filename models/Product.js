const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, 'Product name is required.']
	},
	description: {
		type: String,
		required: [true, 'Description is required.']
	},
	price: {
		type: Number,
		required: [true, 'Password is required.']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [
		{
			userId: {
				type: String,
				required: [true, 'User ID is required.']
			},
			orderId: {
				type: String,
				required: false
			}
		}
	],
	userCart: [
		{
			userId: {
				type: String,
				required: [true, 'User ID is required.']
			},
			cartId: {
				type: String,
				required: false
			}
		}
	]
});

module.exports = mongoose.model('Product', productSchema);